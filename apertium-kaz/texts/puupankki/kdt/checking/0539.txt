"<Неліктен>"
	"неліктен" PRON @nmod #1->5
	"неліктен" adv @nmod #1->5
"<оның>"
	"ол" PRON @nsubj #2->5
	"ол" prn dem gen @nsubj #2->5
	"ол" prn pers p3 sg gen @nsubj #2->5
"<сұлу>"
	"сұлу" adj @amod #3->4
"<қызға>"
	"қыз" n dat @iobj #4->5
"<құмар>"
	"құмар" adj @dobj #5->9
	"құмар" adj @ccomp #5->9
"<боп>"
	"бол" v iv prc_perf @cop #6->5
"<жүргенін>"
	"жүр" vaux ger_past px3sp acc @aux #7->5
"<кейін>"
	"кейін" adv @advmod #8->9
"<естірсіз>"
	"есті" VERB @root #9->0
	"есті" v tv fut p2 frm sg @root #9->0
"<.>"
	"." sent @punct #10->9
¶
